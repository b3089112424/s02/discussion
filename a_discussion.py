# Create and traverse through Binary Tree
# Binary class will define the structure of a Node
class BinaryTree:
	# initialize the attributes of BinaryTree
	def __init__(self, data):
		self.data = data
		self.left = None
		self.right = None
		self.size = 0

	# Create methods for Binary Tree

	# Preorder, you traverse from the root to the left subtree then to the right subtree
	# Root > Left > Right
	def preorder_print(self, root):
		# if there is a data
		if root is not None:
			self.size += 1
			print(root.data, end=" -> ")

			# recursion, calling the method inside the method
			self.preorder_print(root.left)
			self.preorder_print(root.right)

	# Counting the number of elements in the tree
	def get_size(self):
		if self.size == 0:
			return 'The size of the tree is: 0'
		return 'The size of the tree is {size}'.format(size = self.size)
		# return 'The size of the tree is {self.size}'

	# Checking if the tree is not empty (or has elements)
	def is_empty(self):
		if self.size == 0:
			return True
		else:
			return False

	# Getting the height of the binary tree
	def height(self, root):
		if root == None:
			self.size == 0
			return 0
		if self.is_empty():
			return max(self.height(root.left), self.height(root.right)) + 1

	# Inorder, traverses from the left subtree to the root then to the right subtree
	# Left > Root > Right
	def inorder_print(self, root):
		if root is not None:
			self.size += 1
			self.inorder_print(root.left)
			print(root.data, end=" -> ")
			self.inorder_print(root.right)

	# Postorder, traverses from the left subtree to the right subtree then to the root
	def postorder_print(self, root):
		if root is not None:
			self.size += 1
			self.postorder_print(root.left)
			self.postorder_print(root.right)
			print(root.data, end=" -> ")

	def deletion(self, root, key):
		# if binary tree is empty
		if root == None:
			return None

		# if there is no children and if the key is equal to the root
		if root.left == None and root.right == None:
			if root.data == key:
				# returns the search term
				return key
			else:
				# if search term not found, return an error message
				print("Key not found")
				return "error"

		key_node = None
		temp = None
		last = None
		q = []
		# adding the root inside the q
		q.append(root)

		while(len(q)):

			# temp variable now contains the first element in q list
			temp = q.pop(0)

			# temp.data is the value of root.data
			if temp.data == key:
				# key_node is now equal to root
				key_node = temp

			# temp.left is the value of root.left
			if temp.left:
				last = temp # storing the parent node
				q.append(temp.left)
			if temp.right:
				last = temp
				q.append(temp.right)

		# if key is found
		if key_node != None:
			key_node.data = temp.data

			# removing the deepest node
			if last.right == temp:
				last.right = None
			if last.left == temp:
				last.left = None

		return 'Deleted item is: {item}'.format(item = key)

# declaring the root and children of the binary tree
root = BinaryTree('President')
root.left = BinaryTree('VP for internal')
root.right = BinaryTree('VP for external')
root.left.left = BinaryTree('VPI secretary')
root.left.right = BinaryTree('VPI executive assistant')
root.right.left = BinaryTree('VPE secretary')
root.right.right = BinaryTree('VPE executive assistant')
print(root.deletion(root, 'President'))
root.preorder_print(root)
print('')
# Test the other traversal methods
root.inorder_print(root)
print('')
root.postorder_print(root)
print('')
# get the size of the tree
print(root.get_size())
# using the is_empty() method to check the number of elements
print('Is empty?,', root.is_empty())
# checking the height of the tree
print('Height: ', root.height(root))
