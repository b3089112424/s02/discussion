# Implementation of module
import binarytree as bt
from binarytree import build

nodes = [3, 6, 8, 2, 11, 13]
binary_tree = build(nodes)

# print the binary tree based from the values of the list
print('Binary Tree from list: \n', binary_tree)

# print the values from the list
print('\nList from binary tree :', binary_tree.values)

# traversal using the module
print('Tree in inorder traversal: ', binary_tree.inorder)
print('Tree in preorder traversal: ', binary_tree.preorder)
print('Tree in postorder traversal: ', binary_tree.postorder)

# checking the number of elements
print('The size of the tree is: ', binary_tree.size)

# Height of the tree
print('The height of the tree: ', binary_tree.height)

# leaf - the nodes without children
print('THe leaf count is: ', binary_tree.leaf_count)
print('The leaves are: ', binary_tree.leaves)

# return the nodes per level
print('The levels of the tree: ', binary_tree.levels)

# Balanced binary tree
print('Is tree balance? ', binary_tree.is_balanced)

# Complete binary tree
print('Is this tree a complete tree? ', binary_tree.is_complete)

# Perfect binary tree
print('Is this tree a perfect tree? ', binary_tree.is_perfect)

# Check if BST
print('Is the tree a BST type? ', binary_tree.is_bst)

# Returns the min-heap (least node)
print('The min heap is: ', binary_tree.min_node_value)

# Returns the max-heap (greatest node)
print('The max heap is: ', binary_tree.max_node_value)

# Return all the left nodes tree
print(binary_tree.left)

# Return all the right nodes tree
print(binary_tree.right)

# Check if numbers of nodes in left subtree is equal to right subtree
print(binary_tree.is_symmetric)

# Check if all its non-leaf nodes have both left and right child nodes
print(binary_tree.is_strict)